/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o mananger manager.c

# Description:
# This function keeps track of the percentage of memory in use.
# When the memory exceeds BARRIER %, it prints an alert.
# It then begins to kill processes until the memory usage falls beneath BARRIER.
# When it drops back down, it prints that out as well.  
*/


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

//This is the maximum amount of memory in the system
int const MAX_MEM = 3874092;
//this is the percentage of memory use required to trigger process killing
double const BARRIER = 85;

int main(int argc, char * argv[]){
  //memFile is used to open the command for finding the memory in use
  //pidFile is used to find which processes are currently running and able to be killed
  FILE* memFile, *pidFile;
  //memUsageBuffer is used to read the output from "free"
  //pidBuffer is used to read the output from "ps"
  char memUsageBuffer[1024], pidBuffer[1024];
  //memUsage is the amount of memory in use
  int memUsage = 0;
  //prevMemUsage was the amount of memory used on the last check
  int prevMemUsage = 0;
  //pid is the pid of this process
  int pid = getpid();
  //this will be the command used to find pids that can be killed
  char pidCommand[512];
  
  //"ps -u lnxuser --sort -pid" finds all processes belonging to lnxuser
  // it sorts them by descending pid
  
  // we pipe this to grep, which only shows entries that 
  //   * are not this process, that do not have the same PID
  //   * are not sshd processes, since those are how we access the VM
  //   * are not the bash
  //   * are not grep or ps or awk (i.e., are not part of this command)
  //   * are not sh, which is used to execute some of our commands
  // -v makes it so we show only entries that DO NOT contain these phrases
  
  // awk sans through and shows the first column in a line with a number 
  //   * /[0-9]/ is a regular expression to find numbers
  snprintf(pidCommand, sizeof(pidCommand), "ps -u lnxuser --sort -pid | \
                        grep \
                        -e %d \
                        -e sshd \
                        -e bash \
                        -e grep \
                        -e ps \
                        -e awk \
                        -e ps \
                        -v | \
                        awk '/[0-9]/ { print $1 }'", pid);
                        
  
  //we run this indefinitely because this code 
  //  is supposed to run in the background and monitor memory use  
  while(1){

    /* Open the command for reading. */
    //we use the command "free" which we then pipe into "awk"
    //awk parses the string so that the command only returns the 3rd column after the output "Mem:"
    //this column is the current memory usage
    //thus, this command returns a string of the current memory usage
    memFile = popen("/bin/free | awk '/Mem:/ { print $3 }'", "r");
    //if the file, memFile, is null, terminate and warn that the command failed
    if (memFile == NULL) {
      printf("Failed to run command\n" );
      exit(1);
    }
  
    //read the output of the command
    //this should only loop once, reading in the current usage and storing the prevUsage
    while (fgets(memUsageBuffer, sizeof(memUsageBuffer)-1, memFile) != NULL) {
      prevMemUsage = memUsage; 
      //this converts the c-string output to an int
      memUsage = atoi(memUsageBuffer);  
    } 
  
    /* close the file that accessed the command */
    pclose(memFile); 
    
    //we calculate the amount of memory that was used and the amount currently used.
    //previous percentage of memory used
    double prevPercentUsed = 100*prevMemUsage/(double)MAX_MEM;
    //current percentage of memory used
    double percentUsed = 100*memUsage/(double)MAX_MEM;
    
    //optionally print the current percent usage.  
    //this is helpful for helping to gauge how much memory processes use
    //monitoring this helps us to not overload the VM with processes
    //printf("Used: %f\n", percentUsed);
    
    //if the memory usage exceeds the barrier, and previously did not, warn the user
    if(prevPercentUsed < BARRIER && percentUsed > BARRIER){
      printf("MEMORY USAGE EXCEEDS %.3f\%\n", BARRIER);
    }
    //if the memory previously was above the barrier, and now is not, let the user know
    else if (prevPercentUsed > BARRIER && percentUsed < BARRIER){
      printf("Memory beneath %.3f\%\n", BARRIER);
    }
    
    //if the memory usage exceeds the marrier, start killing processes
    if(percentUsed > BARRIER){
      //open the "ps" command defined above 
      pidFile = popen(pidCommand, "r");
        
      //read the first line of the contents of the output into pidBuffer
      //this will get the killable process with the highest pid                  
      fgets(pidBuffer, sizeof(pidBuffer)-1, pidFile);
      //close the file that access the "ps" command defined above
      pclose(pidFile); 
      
      //kill the process id we read in from the output 
      // the signal 9 translates to SIGKILL
      kill((pid_t)atoi(pidBuffer), 9);
      //Continue to the top of the while loop
      //this way, we do not wait any time before possibly trying to kill another process
      continue; 
    }

     
    //sleep for .25seconds so that we do not check unnecessarily rapidly
    usleep(250000);
  }
  return 0;
} 	