/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o stackTest stackTest.c

# Description:
# This is an empty program simply to test how much memory is allocated on the stack.
*/

#include <stdlib.h>

int main(){
  return 0;
}