/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o heapAndStack heapAndStack.c

# Description:
# This program is meant to give examples for stack AND heap memory allocation.
# At the creation of the process, the stack memory should be allocated.
# After a period fo waiting, the heap memory should be allocated.
# This helps us to see how stack memory and heap memory allocation contrast.
*/


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

//allocates 250k ints to stack and 500k ints to heap
// i.e. 1 million bytes to stack and 2 million to heap
int main(int argc, char * argv[]){
  //allocated a large array to stack
  int bigStack[250000];
  //use all of the array so that the memory must actually be allocated
  for(int i = 0; i < 250000; i++){
    bigStack[i] = i;
  }
  sleep(5);
  //after waiting, dynamically allocate a large array to the heap
  int *ptr = (int *) malloc(500000 * sizeof(int));
  //use all of the array in order to ensure the memory is actually allocated
  for(int i = 0; i < 500000; i++){
    ptr[i] = i;
  }
  //sleep a little to give us a chance to observe the state of the VM
  sleep(10);
  //deallocate the heap memory to avoid memory leaks
  if(ptr != NULL) free(ptr);
} 	