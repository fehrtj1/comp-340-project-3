# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: Not applicable

# Description:
# create any executable and name it "spawn"
# call this function with the number of processes you want to spawn
# ex. "bash spawnMany.bash 100" starts the ./spawn process 100 times

echo "Spawning $1 processes"
for ((i = 1; i <= $1; i++)); 
do
    ( ./spawn & )
done
