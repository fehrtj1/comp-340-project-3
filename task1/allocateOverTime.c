/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o allocateOverTime allocateOverTime.c

# Description:
# Here we allocate 2 million bytes to the heap and then immediately use it
# We do this several times, waiting a few seconds in between each allocation and use.
# This way we can observe how identical allocation changes over time.  
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

//2 million bytes of dynamically allocated
//very couple of seconds
int main(int argc, char * argv[]){

  //allocate .5million ints (2million bytes) to the heap
  int *ptr1 = (int *) malloc(500000 * sizeof(int));
  //make sure the memory is allocated by actually using it
  for(int i = 0; i < 500000; i++){
    ptr1[i] = i;
  }
  //output some text to keep track of where we are in the program
  printf("allocated 1\n");
  //flush stdout so that our print statements occur in sequence
  //otherwise they wait for all sleep statements
  fflush(stdout);
  //wait a few seconds
  sleep(5);
  
  //allocate .5million ints (2million bytes) to the heap
  int *ptr2 = (int *) malloc(500000 * sizeof(int));
  //make sure the memory is allocated by actually using it
  for(int i = 0; i < 500000; i++){
    ptr2[i] = i;
  }
  printf("allocated 2\n");
  fflush(stdout);
  //wait a few seconds
  sleep(5);
  
  //allocate .5million ints (2million bytes) to the heap
  int *ptr3 = (int *) malloc(500000 * sizeof(int));
  //make sure the memory is allocated by actually using it
  for(int i = 0; i < 500000; i++){
    ptr3[i] = i;
  }
  printf("allocated 3\n");
  //flush stdout so that our print statements occur in sequence
  fflush(stdout);
  //wait for a few seconds
  sleep(5);
  
  //allocate .5million ints (2million bytes) to the heap
  int *ptr4 = (int *) malloc(500000 * sizeof(int));
  //make sure the memory is allocated by actually using it
  for(int i = 0; i < 500000; i++){
    ptr4[i] = i;
  }
  printf("allocated 4\n");
  //flush stdout so that our print statements occur in sequence
  fflush(stdout);
  
  //wait for a few seconds
  sleep(5);
  
  //deallocate all these in order to avoid memory leaks
  if(ptr1 != NULL) free(ptr1);
  if(ptr2 != NULL) free(ptr2);
  if(ptr3 != NULL) free(ptr3);
  if(ptr4 != NULL) free(ptr4);
  printf("de-allocated\n");
} 	