/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o emptyProcess emptyProcess.c

# Description:
# This process does absolutely nothing.
# This program should be an example of what is "minimally" needed to run a C program that only sleeps
# We never allocate anything on the heap, and there is little else in this program.
# We sleep in order to have time to observe the state of memory.
*/

#include <unistd.h>
//This process does nothing, so we can hopefully see how much is allocated for an "empty" process
int main(int argc, char * argv[]){
  //wait for a couple of seconds
  sleep(10);
} 	