/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o delayedHeap delayedHeap.c

# Description:
# This simply allocates some memory to the heap after a period of waiting.
# We wait before calling malloc, then wait before using the array we create.
*/

#include <unistd.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
  //we wait a few seconds to allocate anything at all.  
  sleep(5);
  //after waiting, allocate to the heap
  int *ptr = (int *) malloc(500000 * sizeof(int));
  //we now wait a bit to see if memory is allocated upon malloc or upon use
  sleep(5);
  //after a few seconds, we actually use the memory on the heap
  for(int i = 0; i < 500000; i++){
    ptr[i] = i;
  }
  //we wait a little bit
  sleep(5);
  //we free memory to avoid memory leaks
  if(ptr != NULL) free(ptr);
  sleep(2);
} 	