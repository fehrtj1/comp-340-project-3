/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o printUsage printUsage.c

# Description:
# This will print the number of kilobytes of memory in use, according to "free"
# it will also print the change from the last time it was polled.
# This script runs indefinitely until interrupted as an aid for monitoring memory usage changes
*/


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
  
  FILE *fp;
  char path[1024];
  int memUsage = 0;
  int prevMemUsage = 0;
  
  //Constantly check for memory usage
  while(1){
    /* Open the command for reading. */
    fp = popen("/bin/free | awk '/Mem:/ { print $3 }'", "r");
    if (fp == NULL) {
      printf("Failed to run command\n" );
      exit(1);
    }
  
    /* Store the value returned by the free and awk commands */
    while (fgets(path, sizeof(path)-1, fp) != NULL) {
      //keep track of the past in order to calculate the diff
      prevMemUsage = memUsage;
      //convert the string to an integer representation of the kilobytes used
      memUsage = atoi(path);
      //print out the memory usage and the change in memory usage.
      printf("%d \t %d\n", memUsage, memUsage - prevMemUsage);
    }
    
    
  
    /* close */
    pclose(fp);
    
    //wait .25 seconds before checking the memory and memory diff again
    //this makes the feed more readable
    usleep(250000);
  }
  return 0;
} 	