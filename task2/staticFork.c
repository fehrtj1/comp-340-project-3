/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o staticFork staticFork.c

# Description:
# This function forks a variable number of processes.
# Each of these processes have a large, statically allocated array.
# This way we can observe how processes are allocated memory on the stack when forked. 
*/

#include<stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
  //if no argument given, do not try to complete the function
  if(argc < 2){
    printf("Please give the number of processes as an argument\n");
    return 1;
  }
  //numPids = the num of processes given by the user
  int numPids = atoi(argv[1]);
  //the size of the arrays to be allocated
  const int SIZE = 2000000;
  
  //the pids created for the numPids
  pid_t* pids = (pid_t*) malloc(numPids * sizeof(pid_t));
  //last is to keep track of which pid was last inspected in the below for loop
  pid_t last;

  //Give the parent a small statically allocated array
  int small[100];
  for(int i = 0; i < 100; i++){
    small[i] = i;
  }
  /* fork child processes */
  
  //fork however many processes the user specified
  for(int i = 0; i < numPids; i++){
    pids[i] = fork();
    //keep track of the last pid in order to let the parent wait() later on
    last = pids[i];
    
    //check if an error occured
    if (pids[i] < 0) { 
      fprintf(stderr, "Fork Failed");
      return 1;
    }
    //if this is the child process, break out of the for loop
    //this way, we avoid children forking more children
    else if (pids[i] == 0) { /* child process */
      //use a statically allocated array of size SIZE.  
      int bigArray[SIZE];
      //trivially use the array in order to force the memory to be allocated
      for(int i = 0; i < SIZE; i++){
        bigArray[i] = i;
      }
      printf("child: pid = %d\n", getpid()); 
      //let the process idle for a while to help us monitor its state
      sleep(30);
      //break to avoid forking off of this child
      break;
    } 
    
  }
  //if the last process was the parent, then this process is the parent
  // wait for children processes
  if(last > 0){
    wait(NULL);
  }

  return 0;
}
