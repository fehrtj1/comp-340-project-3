/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o mallocFork mallocFork.c

# Description:
# This function forks a variable number of processes.
# Each of these processes have a large, dynamically allocated array.
# This way we can observe how processes are allocated memory on the heap when forked. 
*/

#include<stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
int main(int argc, char * argv[]){
  //if no arguement given, do not try to complete the function
  if(argc < 2){
    printf("Please give the number of processes as an argument\n");
    return 1;
  }

  //numPids = the num of processes given by the user
  int numPids = atoi(argv[1]);
  //the size of the arrays to be allocated
  const int SIZE = 2000000;

  //the pids created for the numPids
  pid_t* pids = (pid_t*) malloc(numPids * sizeof(pid_t));
  //last is to keep track of which pid was last inspected in the below for loop
  pid_t last;

  /* fork child processes */
  
  //fork numPids processes
  for(int i = 0; i < numPids; i++){
    //fork each process
    pids[i] = fork();
    //keep track of the most recent pid
    last = pids[i];
    //check if an error has occured
    if (pids[i] < 0) { 
      fprintf(stderr, "Fork Failed");
      return 1;
    }
    //check if this process is the child process
    else if (pids[i] == 0) {
      //if this is the child, dynamically allocate a SIZE sized array called bigArray
      int* bigArray = (int *) malloc(SIZE * sizeof(int));
      //trivially use the array to ensure it is allocated
      for(int i = 0; i < SIZE; i++){
        bigArray[i] = i;
      }
      printf("child: pid = %d\n", getpid()); /* B */
      sleep(30);
      //free the array
      free(bigArray);
      //break to avoid forking off of this child
      break;
    } 
    
  }
  
  //if the last process was the parent, then this process is the parent
  // wait for children processes
  if(last > 0){
    wait(NULL);
  }

  return 0;
}
