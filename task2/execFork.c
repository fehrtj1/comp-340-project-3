/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o execFork execFork.c

# Description:
# This function forks a variable number of processes.
# Each of these processes have a large, statically allocated array.
# This way we can observe how processes are allocated memory on the stack when forked. 
*/

#include<stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
  //numPids = the num of processes given by the user
  int numPids = atoi(argv[1]);
  //the size of the arrays to be allocated
  const int SIZE = 2000000;
  //if no number of processes and no command to execv given, do not try to complete the function
  if(argc < 3){
    printf("Please give the number of processes as an argument, as well as the process you would like to exec (no params, please).\n");
    return 1;
  }
  //the pids created for the numPids
  pid_t* pids = (pid_t*) malloc(numPids * sizeof(pid_t));
  //last is to keep track of which pid was last inspected in the below for loop
  pid_t last;
  //the list or parameters for running this test
  char *const parmList[] = { NULL };


  //create a small array just to use some memory, and have something allocated
  int small[100];
  //trivially use the memory to ensure it is allocated
  for(int i = 0; i < 100; i++){
    small[i] = i;
  }
  /* fork child processes */
  
  //fork numPids processes
  for(int i = 0; i < numPids; i++){
    //fork each process
    pids[i] = fork();
    //keep track of the most recent pid
    last = pids[i];
    //check if an error has occured
    if (pids[i] < 0) { 
      fprintf(stderr, "Fork Failed");
      return 1;
    }
    //check if this process is the child process
    else if (pids[i] == 0) { /* child process */
      //sleep for a little to observe the state of the process before execv
      sleep(20);
      //call execv, which should take us out of this program's flow of control
      execv(argv[2], parmList);
      break;
    } 
    
  }
  
  //if the last process was the parent, then this process is the parent
  // wait for children processes
  if(last > 0){
    wait(NULL);
  }
  return 0;
}
