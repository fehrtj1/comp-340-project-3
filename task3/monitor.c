/*
# Luke Meier and Tyler Fehr
# Course No: COMP 340 A
# Date: 12/8/17
# Compile with: gcc -o monitor monitor.c

# Description:
# This function keeps track of the percentage of memory in use.
# When the memory exceeds BARRIER %, it prints an alert.
# When it drops back down, it prints that out as well.  
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

//This is the maximum amount of memory in the system
int const MAX_MEM = 3874092;
//this is the percentage of memory use required to trigger the alert
double const BARRIER = 85;

int main(int argc, char * argv[]){
  //fp is the file used to run proceses using popen
  FILE *fp;
  //buffer is used to read in the values in fp
  char buffer[1024];
  //memUsage stores the amount of memory currently in use
  int memUsage = 0;
  //prevMemUsage store the amount of memory in use at the previous check
  int prevMemUsage = 0;

  //We run this code infinitely, since it is meant to constantly monitor memory use
  while(1){

    /* Open the command for reading. */
    //we use the command "free" which we then pipe into "awk"
    //awk parses the string so that the command only returns the 3rd column after the output "Mem:"
    //this column is the current memory usage
    //thus, this command returns a string of the current memory usage
    fp = popen("/bin/free | awk '/Mem:/ { print $3 }'", "r");
    //if the file, fp, is null, terminate and warn that the command failed
    if (fp == NULL) {
      printf("Failed to run command\n" );
      exit(1);
    }
  
    //read the output of the command
    //this should only loop once, reading in the current usage and storing the prevUsage
    while (fgets(buffer, sizeof(buffer)-1, fp) != NULL) {
      prevMemUsage = memUsage;
      //atoi converts the c-string to an int
      memUsage = atoi(buffer);
    }
    
    /* close the file that accessed the process */
    pclose(fp);
    
    //we calculate the amount of memory that was used and the amount currently used.
    //previous percentage of memory used
    double prevPercentUsed = 100*prevMemUsage/(double)MAX_MEM;
    //current percentage of memory used
    double percentUsed = 100*memUsage/(double)MAX_MEM;
    
    //optionally print the current percent usage.  
    //this is helpful for helping to gauge how much memory processes use
    //monitoring this helps us to not overload the VM with processes
    //printf("Used: %f\n", percentUsed);
    
    //if the memory usage exceeds the barrier, and previously did not, warn the user
    if(prevPercentUsed < BARRIER && percentUsed > BARRIER){
      printf("MEMORY USAGE EXCEEDS %.3f\%\n", BARRIER);
    }
    //if the memory previously was above the barrier, and now is not, let the user know
    else if (prevPercentUsed > BARRIER && percentUsed < BARRIER){
      printf("Memory beneath %.3f\%\n", BARRIER);
    }
    
    //sleep for .25seconds so that we do not check unnecessarily rapidly
    usleep(250000);
  }
  return 0;
} 	